package pl.rentacar.entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    @JoinColumn(name = "customerId")
    private Customer customer;
    @OneToOne
    private Car car;
    private LocalDateTime dateFrom;
    private LocalDateTime dateTo;
    @OneToOne
    private Branch rentFrom;
    @OneToOne
    private Branch returnTo;
    private double amountPaidIn;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public LocalDateTime getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDateTime getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDateTime dateTo) {
        this.dateTo = dateTo;
    }

    public Branch getRentFrom() {
        return rentFrom;
    }

    public void setRentFrom(Branch rentFrom) {
        this.rentFrom = rentFrom;
    }

    public Branch getReturnTo() {
        return returnTo;
    }

    public void setReturnTo(Branch returnTo) {
        this.returnTo = returnTo;
    }

    public double getAmountPaidIn() {
        return amountPaidIn;
    }

    public void setAmountPaidIn(double amountPaidIn) {
        this.amountPaidIn = amountPaidIn;
    }
}
