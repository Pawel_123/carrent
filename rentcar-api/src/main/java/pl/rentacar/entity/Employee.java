package pl.rentacar.entity;

import javax.persistence.*;

@Entity
@Inheritance
public class Employee extends User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String secondName;
    private String position;
    private String email;
    private String address;
    @ManyToOne
    @JoinColumn(name = "branchId")
    private Branch branch;
    private Role role;

    public Employee(String firstName, String secondName, String position, String email,String address, Branch branch, Role role) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.position = position;
        this.email = email;
        this.address = address;
        this.branch = branch;
        this.role = role;
    }


    public Employee() {
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String getRole() {
        return this.role.name();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }
}
