package pl.rentacar.entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class CarBack {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @OneToOne
    private Employee employee;
    private LocalDateTime carBackDate;
    @OneToOne
    private Reservation reservation;
    private double additionalPayment;
    private String comments;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public LocalDateTime getCarBackDate() {
        return carBackDate;
    }

    public void setCarBackDate(LocalDateTime carBackDate) {
        this.carBackDate = carBackDate;
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    public double getAdditionalPayment() {
        return additionalPayment;
    }

    public void setAdditionalPayment(double additionalPayment) {
        this.additionalPayment = additionalPayment;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
