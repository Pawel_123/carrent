package pl.rentacar.db;

import io.codearte.jfairy.Fairy;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import pl.rentacar.entity.Branch;
import pl.rentacar.entity.Employee;
import pl.rentacar.entity.Rental;
import pl.rentacar.entity.User;
import pl.rentacar.repository.BranchRepository;
import pl.rentacar.repository.EmployeeRepository;
import pl.rentacar.repository.RentalRepository;

@Component
@ConditionalOnProperty(name = "carrent.db.recreate", havingValue = "true")
public class DbSeeder implements CommandLineRunner {
    private RentalRepository rentalRepository;
    private BranchRepository branchRepository;
    private EmployeeRepository employeeRepository;

    public DbSeeder(RentalRepository rentalRepository, BranchRepository branchRepository, EmployeeRepository employeeRepository) {
        this.rentalRepository = rentalRepository;
        this.branchRepository = branchRepository;
        this.employeeRepository = employeeRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        rentalRepository.deleteAll();
        branchRepository.deleteAll();
        employeeRepository.deleteAll();
        //name generator
        Fairy fairy = Fairy.create();

        //create Rental company
        rentalRepository.save(new Rental("Fast Car", "www.fastcar.org", "Warszawa", "Al. Ujazdowskie 23", "Jan Kowalski", "IMG_LOGOFASTCAR"));
        //create list of branches
        for (int i = 0; i < 5; i++) {
            branchRepository.save(new Branch(fairy.person().getAddress().getCity(), fairy.person().getAddress().getStreet()));
        }
        //create list of managers
        for (int i = 1; i < 6; i++) {
            Long num = (long) i;
            employeeRepository.save(new Employee(fairy.person().getFirstName(), fairy.person().getLastName(), "Manager", fairy.person().getEmail(),fairy.person().getAddress().getAddressLine1(), branchRepository.findById(num).get(), User.Role.MANAGER));
        }
        //create list of Employees
        for (int i = 1; i < 6; i++) {
            for (int j = 0; j < 10; j++) {
                employeeRepository.save(new Employee(fairy.person().getFirstName(), fairy.person().getLastName(), "Employee", fairy.person().getEmail(),fairy.person().getAddress().getAddressLine1(), branchRepository.findById((long) i).get(), User.Role.EMPLOYEE));
            }
        }

        System.out.println("Database Initialized");


    }
}
