package pl.rentacar.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.rentacar.entity.Branch;

@Repository
public interface BranchRepository extends JpaRepository<Branch, Long> {

}
