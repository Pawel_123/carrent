package pl.rentacar.dto;

import javax.validation.constraints.NotNull;

public class BranchDto {

    private String id;
    @NotNull
    private String city;
    @NotNull
    private String address;
    private int nbOfEmployees;
    private int nbOfCars;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getNbOfEmployees() {
        return nbOfEmployees;
    }

    public void setNbOfEmployees(int nbOfEmployees) {
        this.nbOfEmployees = nbOfEmployees;
    }

    public int getNbOfCars() {
        return nbOfCars;
    }

    public void setNbOfCars(int nbOfCars) {
        this.nbOfCars = nbOfCars;
    }
}
