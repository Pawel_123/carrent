package pl.rentacar.dto;

import javax.validation.constraints.NotNull;

public class EmployeeDto {

    private String id;
    @NotNull
    private String firstName;
    @NotNull
    private String secondName;
    @NotNull
    private String position;
    @NotNull
    private String email;
    @NotNull
    private String address;
    @NotNull
    private String branchId;
    @NotNull
    private String role;

    public EmployeeDto() {
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getPosition() {
        return position;
    }

    public String getEmail() {
        return email;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
