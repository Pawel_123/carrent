package pl.rentacar.controller;


import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;
import pl.rentacar.dto.EmployeeDto;
import pl.rentacar.entity.Branch;
import pl.rentacar.entity.Employee;
import pl.rentacar.entity.User;
import pl.rentacar.repository.BranchRepository;
import pl.rentacar.repository.EmployeeRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/employees")
@CrossOrigin
public class EmployeeController {
    private EmployeeRepository employeeRepository;
    private BranchRepository branchRepository;
    private ModelMapper mapper;

    public EmployeeController(EmployeeRepository employeeRepository, BranchRepository branchRepository) {
        this.employeeRepository = employeeRepository;
        this.branchRepository = branchRepository;
        this.mapper = new ModelMapper();
    }

    @GetMapping
    public List<Employee> get() {
        return employeeRepository.findAll();
    }

    @GetMapping("/byBranch/{id}")
    public List<Employee> getAllFromBranch(@PathVariable Long id) {
        List<Employee> employees = new ArrayList<>();
        Optional<Branch> branch = branchRepository.findById(id);
        if (branch.isPresent()) {
            employees = employeeRepository.findAllByBranch(branch.get());
        }
        return employees;
    }
    @PostMapping
    public Employee add(@RequestBody EmployeeDto employeeDto){
        Branch branch = branchRepository.findById(Long.valueOf(employeeDto.getBranchId())).get();
        Employee employee = new Employee(employeeDto.getFirstName(),employeeDto.getSecondName(),employeeDto.getPosition(),employeeDto.getEmail(),employeeDto.getAddress(),branch, User.Role.valueOf(employeeDto.getRole()));
        return employeeRepository.save(employee);

    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
        employeeRepository.deleteById(id);
    }
}
