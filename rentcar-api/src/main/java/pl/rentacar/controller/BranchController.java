package pl.rentacar.controller;

import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;
import pl.rentacar.dto.BranchDto;
import pl.rentacar.entity.Branch;
import pl.rentacar.repository.BranchRepository;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/branches")
@CrossOrigin
public class BranchController {
    private BranchRepository branchRepository;
    private ModelMapper mapper;

    public BranchController(BranchRepository branchRepository) {
        this.branchRepository = branchRepository;
        mapper = new ModelMapper();
    }

    @GetMapping
    public List<BranchDto> get() {
        List<Branch> branches = branchRepository.findAll();
        return branches.stream()
                .map(branch -> mapper.map(branch, BranchDto.class)).collect(Collectors.toList());
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        branchRepository.deleteById(id);
    }

    @PostMapping
    public Branch add(@RequestBody BranchDto branchDto) {
        Branch branch = mapper.map(branchDto, Branch.class);
        return branchRepository.save(branch);
    }

    @PutMapping("/{id}")
    public Branch modify(@RequestBody Branch branch, @PathVariable Long id) {
        Branch branchToModify = branchRepository.findById(id).get();
        branchToModify.setCity(branch.getCity());
        branchToModify.setAddress(branch.getAddress());
        branchToModify.setCars(branch.getCars());
        branchToModify.setEmployees(branch.getEmployees());
        branchRepository.save(branchToModify);
        return branchToModify;
    }


}
