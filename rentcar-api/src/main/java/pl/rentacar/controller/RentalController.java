package pl.rentacar.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.rentacar.entity.Rental;
import pl.rentacar.repository.RentalRepository;

import java.util.List;

@RestController
@RequestMapping("/rentals")
@CrossOrigin
public class RentalController {
    private RentalRepository rentalRepository;

    public RentalController(RentalRepository rentalRepository) {
        this.rentalRepository = rentalRepository;
    }

    @GetMapping
    public List<Rental> get() {
        return rentalRepository.findAll();
    }

    @PostMapping
    public Rental create(@RequestBody Rental rental) {
        return rentalRepository.save(rental);
    }
}
