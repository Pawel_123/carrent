package pl.rentacar.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.rentacar.entity.Branch;
import pl.rentacar.entity.Car;
import pl.rentacar.repository.BranchRepository;
import pl.rentacar.repository.CarRepository;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/car")
@CrossOrigin
public class CarController {

    private CarRepository carRepository;
    private BranchRepository branchRepository;

    public CarController(CarRepository carRepository, BranchRepository branchRepository) {
        this.carRepository = carRepository;
        this.branchRepository = branchRepository;
    }

    @GetMapping
    public List<Car> get() {
        return carRepository.findAll();
    }

    @GetMapping("/byBranch/{id}")
    public List<Car> getByBranch(@PathVariable Long id) {
        Optional<Branch> branch = branchRepository.findById(id);
        return carRepository.findAllByBranch(branch.get());
    }
    @PostMapping
    public Car post(@RequestBody Car car){
        return carRepository.save(car);
    }

    @PutMapping("/{id}")
    public void put(@RequestBody Car car, @PathVariable Long id) {
        Car carToChange = carRepository.findById(id).get();
        carToChange.setBranch(car.getBranch());
        carToChange.setStatus(car.getStatus());
        carToChange.setCourse(car.getCourse());
        carToChange.setPriceForADay(car.getPriceForADay());
        carRepository.save(carToChange);

    }
}
