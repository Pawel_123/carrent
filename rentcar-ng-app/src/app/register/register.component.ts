import {Component, OnInit} from '@angular/core';
import {Branch} from "../branches/model/branch";
import {ApiService} from "../shared/api.service";
import {Employee} from "../employees/model/employee";
import {Role} from "../employees/model/role.enum";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  branches: Branch[] = [];
  employee: { id: null; firstName: null; secondName: null; position: null; email: null; address: null; branchId: null } =
    { id: null, firstName: null, secondName: null, position: null, email: null, address: null, branchId: null };
  keys() : Array<string> {
    let keys = Object.keys(Role);
    return keys.slice(keys.length / 2);
  }




  constructor(private apiService: ApiService) {
  }

  ngOnInit() {
    this.getAllBranches();
  }


  getAllBranches() {
    this.apiService.getAllBranches().subscribe(
      res => {
        this.branches = res;
      },
      err => {
        alert("Error has occured")
      }
    );
  }

}
