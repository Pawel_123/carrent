import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NavigationComponent} from './navigation/navigation.component';
import {LoginComponent} from './login/login.component';
import {BranchesComponent} from './branches/branches.component';
import {CarsComponent} from './cars/cars.component';
import {RegisterComponent} from './register/register.component';
import {Router, RouterModule, Routes} from "@angular/router";
import {NotFoundComponent} from './not-found/not-found.component';
import {HomeComponent} from './home/home.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {EmployeesComponent} from './employees/employees.component';
import {RegisterCustomerComponent} from './register-customer/register-customer.component';


const appRoute: Routes = [
  {
    path: 'branches',
    component: BranchesComponent
  },
  {
    path: 'cars',
    component: CarsComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'register/customer',
    component: RegisterCustomerComponent
  },

  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full'
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    LoginComponent,
    BranchesComponent,
    CarsComponent,
    RegisterComponent,
    NotFoundComponent,
    HomeComponent,
    EmployeesComponent,
    RegisterCustomerComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoute)
    //, {enableTracing: true}
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
