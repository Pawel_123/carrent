import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Branch} from "../branches/model/branch";
import {UserViewModel} from "../login/login.component";
import {Employee} from "../employees/model/employee";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private BASE_URL = "http://localhost:8080";
  private LOGIN_URL = `${this.BASE_URL}\\login`;
  private GET_BRANCHES = `${this.BASE_URL}\\branches`;
  private SAVE_BRANCHES = `${this.BASE_URL}\\branches`;
  private UPDATE_BRANCHES = `${this.BASE_URL}\\branches\\`;
  private DELETE_BRANCHES = `${this.BASE_URL}\\branches\\`;
  private GET_ALL_EMPLOYEES = `${this.BASE_URL}\\employees`;
  private GET_EMPLOYEES_BY_BRANCH = `${this.BASE_URL}\\employees\\byBranch\\`;

  constructor(private http: HttpClient) {
  }

  getAllBranches(): Observable<Branch[]> {
    return this.http.get<Branch []>(this.GET_BRANCHES);
  }

  postBranch(branch: Branch): Observable<Branch> {
    return this.http.post<Branch>(this.SAVE_BRANCHES, branch);
  }

  updateBranch(branch: Branch, id: string): Observable<Branch> {
    return this.http.put<Branch>(this.UPDATE_BRANCHES + id, branch);
  }

  deleteBranch(id: string): Observable<any> {
    return this.http.delete(this.DELETE_BRANCHES + id);
  }

  sendLogin(user: UserViewModel): Observable<any> {
    const headers = new HttpHeaders({
      'Authorization': 'Basic ' +
        btoa(user.email + ':' + user.password)
    });
    return this.http.get(this.LOGIN_URL,{headers})
  }

  getAllEmployees(): Observable<Employee []> {
    return this.http.get<Employee []>(this.GET_ALL_EMPLOYEES);
  }

  getEmployeesByBranch(branchId: string): Observable<Employee []> {
    return this.http.get<Employee[]>(this.GET_EMPLOYEES_BY_BRANCH + branchId);
  }
  saveEmployee
}
