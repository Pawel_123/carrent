import {Role} from "./role.enum";

export interface Employee {
  id:string;
  firstName:string;
  secondName:string;
  position:string;
  email:string;
  address:string;
  branchId:string;
  role:Role;

}
