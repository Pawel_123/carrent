export enum Role {
  USER,
  MANAGER,
  ADMIN,
  EMPLOYEE
}
