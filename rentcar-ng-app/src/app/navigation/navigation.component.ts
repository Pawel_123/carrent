import { Component, OnInit } from '@angular/core';
import {ApiService} from "../shared/api.service";

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
isLogged:boolean;
  constructor(private apiService: ApiService) { }

  ngOnInit() {
   this.apiService.sendLogin()
  }

}
