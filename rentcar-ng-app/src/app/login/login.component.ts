import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpClientModule, HttpHeaders} from "@angular/common/http";
import {Router, Routes} from "@angular/router";
import {ApiService} from "../shared/api.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: UserViewModel = {
    email: null,
    password: null
  };
  isLogged:boolean;


  constructor(private apiService: ApiService, private router: Router) {

  }

  ngOnInit() {

  }

  sendLogin(): void {

    this.apiService.sendLogin(this.model).subscribe(
      res => {
        this.isLogged = true;
        console.log("blblblbbjfef")
        this.router.navigate(['/']);
      },
      err => {
        this.isLogged = false;
        alert("Wrong eamil or password")
      }
    )
  }
}

export interface UserViewModel {
  email: string;
  password: string;
}
