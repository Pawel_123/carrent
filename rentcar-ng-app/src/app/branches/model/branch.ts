export interface Branch {
  id:string;
  city:string;
  address:string;
  nbOfEmployees:number;
  nbOfCars:number;
}
