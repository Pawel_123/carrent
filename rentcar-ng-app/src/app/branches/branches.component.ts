import {Component, OnInit} from '@angular/core';
import {ApiService} from "../shared/api.service";
import {Branch} from "./model/branch";
import {Employee} from "../employees/model/employee";
import {Role} from "../employees/model/role.enum";

@Component({
  selector: 'app-branches',
  templateUrl: './branches.component.html',
  styleUrls: ['./branches.component.css']
})
export class BranchesComponent implements OnInit {
  branches: Branch[] = [];
  employees: Employee[] = [];
  selectedBranch: Branch ;
  visibleRow :number = null;

  constructor(private apiService: ApiService) {

  }

  ngOnInit() {
    this.getAllBranches();
    this.getAllEmployees();
  }

  getAllBranches() {
    this.apiService.getAllBranches().subscribe(
      res => {
        this.branches = res;
      },
      err => {
        alert("Error has occured")
      }
    );
  }

  createBranch() {
    let newBranch: Branch = {
      city: 'New_Branch',
      address: null,
      nbOfEmployees: 0,
      nbOfCars: 0,
      id: null
    };
    this.apiService.postBranch(newBranch).subscribe(
      res => {
        newBranch.id = res.id;
      },
      err => {
        alert("Something goes wrong in saving Branch")
      }
    )

  }

  updateBranch(updateBranch: Branch) {

    this.apiService.updateBranch(updateBranch, updateBranch.id).subscribe(
      res => {
      },
      err => {
        alert("Something goes wrong in saving Branch")
      }
    )

  }

  deleteBranch(branch: Branch) {
    if (confirm("Are you sure to delete this branch?")) {
      this.apiService.deleteBranch(branch.id).subscribe(
        res => {
          let inxOfBranch = this.branches.indexOf(branch);
          this.branches.splice(inxOfBranch, 1);

        },
        err => {
          alert("Could not delete this branch");
        }
      )
    }

  }

  getAllEmployees() {
    this.apiService.getAllEmployees().subscribe(
      res => {
        this.employees = res;
      },
      err => {
        alert("Error occured while getting list of all employees");
      }
    )
  }

  deleteEmployee(employee: Employee) {

  }

  addCar() {

  }

  addEmployee() {
    let newEmployee: Employee = {
      id: null,
      firstName: 'First name',
      secondName: 'Second name',
      position: 'Employee',
      email: 'putEmail@something.org',
      address: 'some Address',
      branchId: null,
      role: Role.EMPLOYEE
    }
  }

  selectEmployeesFromBranch(branch: Branch) {
    this.selectedBranch = branch;
    this.apiService.getEmployeesByBranch(branch.id).subscribe(
      res=>{
        this.employees = res;
      },
      err =>{
        alert("Something goes wrong while downloading employees!")
      }
    )

  }
}
